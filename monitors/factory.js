var SystemMonitor = require("./system");
var LogMonitor = require("./log");
var Summary = require("./summary");

module.exports = {
	buildMonitor: function(type, name, opts){
		switch(type.toLowerCase()){
			case "system":
				return new SystemMonitor(opts);
			case "log":
				return new LogMonitor(name, opts);
			case "summary":
				return new Summary(opts);
			default:
				console.log("Undefined monitor type %s", name);
		}
	}
}