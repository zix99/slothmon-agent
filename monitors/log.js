var fs = require("fs");

function LogMonitor(name, opts){
	this._file = opts.file;
	this._sockets = [];
	this._name = name;

	fs.open(opts.file, 'r', function(err, fd){
		if (!err){
			console.log("Start watch on %s", opts.file);
			this._fd = fd;
			this._readBytes = fs.fstatSync(fd).size;
			this._tryRead();
		}
		else
		{
			console.log(err);
		}
	}.bind(this));
}

LogMonitor.prototype = {
	onConnect: function(socket){
		this._sockets.push(socket);

		socket.on("disconnect", function(){
			this._sockets.splice(this._sockets.indexOf(socket), 1);
		}.bind(this));
	},

	getInitialState: function(){
		return {};
	},

	_emitLine: function(line){
		for (var i=0; i<this._sockets.length; i++){
			this._sockets[i].emit('log', {id: this._name, msg: line});
		}
	},

	_tryRead: function(){
		var stats = fs.fstatSync(this._fd);
		if (stats.size < this._readBytes + 1){
			setTimeout(this._tryRead.bind(this), 500);
		}
		else
		{
			fs.read(this._fd, new Buffer(1024), 0, 512, this._readBytes, function(err, bytecount, buff){
				var lines = buff.toString('utf-8', 0, bytecount).split("\n");
				for (var i=0; i<lines.length; i++){
					if (lines[i] !== "")
						this._emitLine(lines[i]);
				}
				this._readBytes += bytecount;
				process.nextTick(this._tryRead.bind(this));
			}.bind(this));
		}
	}
};

module.exports = LogMonitor;