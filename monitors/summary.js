var os = require("os");

function combineMaps(h1, h2){
	var o = {};
	for (var j in h1){
		o[j] = h1[j];
	}
	for (var k in h2){
		o[k] = h2[k];
	}
	return o;
}

function SystemMonitor(opts){
	this._foundation = {
		platform: os.platform(),
		totalmem: os.totalmem(),
		release: os.release()
	};
}

SystemMonitor.prototype = {
	_buildStats: function(){
		var realtime = {
			uptime: os.uptime(),
			loadavg: os.loadavg(),
			freemem: os.freemem()
		};
		return combineMaps(this._foundation, realtime);
	},

	onConnect: function(socket){
		var timer = setInterval(function(){
			socket.emit("summary", this._buildStats());
		}.bind(this), 5000);

		socket.on("disconnect", function(){
			clearInterval(timer);
		});
	},
};

module.exports = SystemMonitor;