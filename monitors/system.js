var os = require("os");

var hostname = os.hostname();
var platform = os.platform();
var totalmem = os.totalmem();

function buildStats()
{
	return {
		"hostname" : hostname,
		"platform" : platform,
		"cpus" : os.cpus(),
		"memory" : {
			"total" : totalmem,
			"free" : os.freemem()
		}
	};
}

function SystemMonitor(opts){

}

SystemMonitor.prototype = {
	onConnect: function(socket){
		var timer = setInterval(function(){
			socket.emit("system", buildStats());
		}.bind(this), 1000);

		socket.on("disconnect", function(){
			clearInterval(timer);
		});
	},
};

module.exports = SystemMonitor;