
module.exports = {
	monitors: {
		"summary" : {
			type: "summary"
		},
		"system" : {
			type: "system"
		},
		"Syslog" : {
			type: "log",
			opts: {
				file: "/var/log/syslog"
			}
		}
	}
}