var app = require("http").createServer(handler)
var io = require("socket.io")(app)
var args = require("minimist")(process.argv.slice(2))
var config = require("./config");
var monitorFactory = require("./monitors/factory");

app.listen(args.port || 23021)

function handler(req, res) {
	res.writeHead(404);
	res.write("Sloth socket.io agent");
	res.end();
}

var monitors = [];
initState = {};

for(var key in config.monitors){
	var item = config.monitors[key];
	var monitor = monitorFactory.buildMonitor(item.type, key, item.opts);
	if (monitor){
		initState[key] = {
			type: item.type,
			opts: (monitor.getInitialState && monitor.getInitialState()) || {},
		};
		monitors.push(monitor);
	}
}

io.on('connection', function(socket){
	console.log("An application connected...");

	socket.emit("state", initState);
	
	for (var i=0; i<monitors.length; i++)
	{
		if (monitors[i].onConnect)
			monitors[i].onConnect(socket);
	}

	socket.on('disconnect', function(){
		console.log("An application disconnected...");
	});
});
